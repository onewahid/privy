import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import groovy.json.JsonSlurper as JsonSlurper

WebUI.callTestCase(findTestCase('000 PRECONDITIONS/001 Randomizer'), [:], FailureHandling.STOP_ON_FAILURE)

response = WS.sendRequest(findTestObject('001 REGISTER/001 REGISTER/001 - POST Register device_type 0', [('url') : GlobalVariable.url, ('rstring') : GlobalVariable.rstring]))

'Define'
def slurper = new JsonSlurper()

def result = slurper.parseText(response.getResponseBodyContent())

def resbody = response.getResponseBodyContent()

def rescode = response.getStatusCode()

def getId = result.get('data').get('user').get('id')

def getPhone = result.get('data').get('user').get('phone')

def getCountry = result.get('data').get('user').get('country')

def getLatlong = result.get('data').get('user').get('latlong')

def getDeviceToken = result.get('data').get('user').get('user_device').get('device_token')

def getDeviceType = result.get('data').get('user').get('user_device').get('device_type')

def getSugarId = result.get('data').get('user').get('sugar_id')

GlobalVariable.sugarId = getSugarId

println(getPhone + ', ' + getCountry + ', ' + getLatlong + ', ' + getDeviceToken + ', ' + getDeviceType)

'Check'
if (rescode == 201 && getPhone == GlobalVariable.rstring && getCountry == GlobalVariable.rstring && getLatlong == GlobalVariable.rstring && getDeviceToken == GlobalVariable.rstring && getDeviceType == 'ios') {
	KeywordUtil.markPassed((('Berhasil dengan code: ' + rescode) + ' dan body: ') + resbody)
} else {
	KeywordUtil.markFailedAndStop((('Gagal dengan code: ' + rescode) + ' dan body: ') + resbody)
}
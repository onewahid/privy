import internal.GlobalVariable

String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTU0123456789"

public static String randomString(String chars, int length) {
  Random rand = new Random();
  StringBuilder sb = new StringBuilder();
  for (int i=0; i<length; i++) {
  sb.append(chars.charAt(rand.nextInt(chars.length())));
  }
  return sb.toString();
}

def randomString = randomString(chars, 5)

println ("Name: " + randomString)

GlobalVariable.rstring = randomString